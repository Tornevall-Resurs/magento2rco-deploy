#!/bin/sh

# Resurs Bank Checkout autodeploy script for Magento 2

reset=0
clone=0
update=0

if [ -f vendor/resursbank/checkout/.git ] ; then

	cd vendor/resursbank/checkout/

	branch=`git branch`
	if [ "$branch" = "* master" ] ; then
		echo "Yes, this is a master branch, so I can pull this without any further restrictions..."
		git reset --hard
		git pull origin master
	fi

else

	if [ "$1" = "reset" ] || [ "$reset" = "1" ] ; then
		echo "Cleaning up repo..."
		rm -rf vendor/resursbank/checkout
		composer up --ignore-platform-reqs
		exit
	fi

	if [ "$1" = "update" ] ; then
		if [ ! -d checkout ] ; then
			update=1
		fi
		if [ -d vendor/resursbank/checkout/.git ] ; then
			cd vendor/resursbank/checkout
			git pull
			exit
		else
			update=1
		fi
	fi

	if [ "$1" = "clone" ] || [ "$clone" = "1" ] ; then
		echo "Cleaning up vendor package..."
		cd vendor/resursbank
		rm -rf checkout
		echo "Cloning from master..."
		git clone git@bitbucket.org:resursbankplugins/resurs-checkout-magento2.git checkout
		if [ "" != "$2" ] ; then
			cd checkout

			echo "Preparing gitrepo..."
			git fetch --all -p >/dev/null 2>&1

			branch=$2
			echo "Checking out branch ${branch}";
			isBranch=`git branch -al | grep ${branch}`
			if [ "" = "$isBranch" ] ; then
				echo "Branch does not exist, do you want to create it?"
				read yn
				if [ "$yn" != "n" ] && [ "$yn" != "N" ] ; then
					git branch ${branch}
					git checkout ${branch}
				fi
			else
				git checkout ${branch}
			fi
		fi
		exit
	fi

	if [ -d vendor/resursbank/checkout ] || [ "$update" = "1" ] ; then
		if [ ! -d vendor/resursbank/checkout/.git ] ; then
			echo "vendor/resursbank/checkout belongs the original Magento package"
			composer up --ignore-platform-reqs
			exit
		fi
	else
		echo "vendor/resursbank/checkout is missing! It should be restored."
		clone=1
	fi

fi
