# RCO autodeploy for Magento 2

This bash script is written to be used manually or from a cronjob. It's built to make it easy to quickly switch between the official repo package (deployed from the Magento repo), and Resurs Bank Bitbucket repos.

You can use following parameters:

    reset           - Restores the rco plugin to its original state (composer package)
    update          - Equals to "composer update", unless the plugin is pointed to bitbucket (in that case, the script will update the git repo instead)
    clone           - Will clean up the rco package, and clone from the official bitbucket repo
    clone <branch>  - Cleans up the rco package, clones from the official bitbucket repo and checks out a branch (if a branch is unexistent, it will ask you to create a new branch)

Send bug reports to [tomas.tornevall@resurs.se](mailto:tomas.tornevall@resurs.se)
